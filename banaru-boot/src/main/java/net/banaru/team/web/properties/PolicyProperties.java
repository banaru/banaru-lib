package net.banaru.team.web.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author banssolin
 */
@ConfigurationProperties(prefix = "banaru.policy")
public class PolicyProperties {

    private boolean mailAuth;

    public boolean isMailAuth() {
        return mailAuth;
    }

    public void setMailAuth(boolean mailAuth) {
        this.mailAuth = mailAuth;
    }

}
