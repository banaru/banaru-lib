package net.banaru.team.web.autoconfiguration;

import java.util.List;

import javax.servlet.Servlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import net.banaru.team.web.properties.LocaleProperties;
import net.banaru.team.web.properties.MessageProperties;
import net.banaru.team.web.utils.MessageUtils;

/**
 * @author banssolin
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({
        Servlet.class, DispatcherServlet.class, WebMvcConfigurer.class
})
@EnableConfigurationProperties({
        LocaleProperties.class, MessageProperties.class
})
@AutoConfigureBefore(WebMvcConfigurer.class)
@Order(Ordered.HIGHEST_PRECEDENCE + 10)
public class MessageConfiguration implements WebMvcConfigurer {

    @Autowired
    private MessageProperties messageProperties;

    @Autowired
    private LocaleProperties LocaleProperties;

    public static final String LOCALE_PARAM = "lang";

    /**
     *
     * "lang"이라는 파라미터가 들어오면 인터셉트 한다.
     *
     * @return {@link LocaleChangeInterceptor}
     */
    // TODO 지원 언어제한
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName(LOCALE_PARAM);
        localeChangeInterceptor.setIgnoreInvalidLocale(true);
        localeChangeInterceptor.setHttpMethods(HttpMethod.GET.name());
        return localeChangeInterceptor;
    }

    // @Bean
    // public SessionLocaleResolver localeResolver() {
    // SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
    // sessionLocaleResolver.setDefaultLocale(Locale.ENGLISH);
    // Timezone: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
    // sessionLocaleResolver.setDefaultTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));
    // return sessionLocaleResolver;
    // }

    @Bean
    public CookieLocaleResolver localeResolver() {
        CookieLocaleResolver localeResolver = new CookieLocaleResolver();
        localeResolver.setDefaultLocale(LocaleProperties.getDefaultLanguage());
        localeResolver.setDefaultTimeZone(LocaleProperties.getTimeZone());
        localeResolver.setCookieName(messageProperties.getCookieName());
        localeResolver.setCookieMaxAge(messageProperties.getCookieMaxAge());
        return localeResolver;
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setCacheSeconds(messageProperties.getCacheSeconds());
        List<String> basenames = messageProperties.getBasenames();
        messageSource.setBasenames(basenames.toArray(new String[basenames.size()]));
        return messageSource;
    }

    /**
     * 인터셉터 등록
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    @Bean
    public MessageUtils messageUtils() {
        return new MessageUtils(messageSource());
    }

}
