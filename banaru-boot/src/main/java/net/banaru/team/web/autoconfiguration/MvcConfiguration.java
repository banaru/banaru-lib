package net.banaru.team.web.autoconfiguration;

import javax.servlet.Servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import net.banaru.team.web.properties.JspViewProperties;
import net.banaru.team.web.properties.MvcProperties;

/**
 * @author banssolin
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({
        Servlet.class, DispatcherServlet.class, WebMvcConfigurer.class
})
@EnableConfigurationProperties({
        MvcProperties.class
})
@AutoConfigureBefore(WebMvcConfigurer.class) // 필수
// org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration에서 사용하는
// Order와 같음.
// @Order를 안주면 Front등에서 WebMvcConfigurerAdapter를 상속받은 환경설정이 먼저되게
// 된다.(AutoConfigure로 등록하는 클래스가 나중에 등록됨)
// AutoConfigure로 등록하는 클래스가 나중에 등록되면 인터페이스 등록의 순서 등이 문제가 생길 수 있다.
// 예를 들면 Front의 Interceptor -> AutoConfigure의 LocaleChangeInterceptor 순으로 등록되어
// 원하는 처리가 Front의 Interceptor가 먼저 캐치 되어, LocaleChangeInterceptor안에서의 처리를 확인하지
// 못하는 경우가 발생할 수 있다.
@Order(Ordered.HIGHEST_PRECEDENCE + 10)
public class MvcConfiguration implements WebMvcConfigurer {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private final JspViewProperties jspViewProperties;

    public MvcConfiguration(MvcProperties mvcProperties) {
        this.jspViewProperties = mvcProperties.getJspView();
    }

    // viewResolver를 재정의
    // 특별한 설정을 안해도 jsp는 움직이는 듯하다.
    // 하지만 jstl을 사용하려면 resolver.setViewClass(JstlView.class); 의 설정을 해줘야 된다(확인 필요)
    // 새로 빈을 올리면 spring.mvc.view(WebMvcProperties)의 설정이 안먹히게 된다.
    // bean 이름은 아무거나 상관없는듯
    @Bean
    // (Properties exists and value is true) = true, (Properties not exists) = true
    // (Properties exists and value is false) = false
    @ConditionalOnProperty(name = "banaru.mvc.jsp-view.enabled", matchIfMissing = true)
    public InternalResourceViewResolver setupViewResolver() {
        logger.info("Register bean of JSTL view resolver.");
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix(jspViewProperties.getPrefix());
        resolver.setSuffix(jspViewProperties.getSuffix());
        resolver.setViewClass(JstlView.class);
        resolver.setOrder(Ordered.LOWEST_PRECEDENCE);
        // jsp에서 bean을 바로 가져다 쓰기 위해서
        resolver.setExposeContextBeansAsAttributes(true);
        return resolver;
    }

}
