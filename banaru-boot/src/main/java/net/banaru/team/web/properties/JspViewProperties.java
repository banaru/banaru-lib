package net.banaru.team.web.properties;

/**
 * @author banssolin
 */
public class JspViewProperties {

    private boolean enabled = true;

    private String prefix = "/WEB-INF/views";

    private String suffix = ".jsp";

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

}
