package net.banaru.team.web.properties;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author banssolin
 */
@ConfigurationProperties("banaru.locale")
public class LocaleProperties {

    private Locale defaultLanguage = Locale.JAPANESE;

    private List<Locale> supportedLanguages = new ArrayList<>();

    private String defaultCountryCode;

    private List<String> supportedCountryCodes = new ArrayList<>();

    private TimeZone timeZone = TimeZone.getTimeZone("Asia/Tokyo");

    public Locale getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Locale defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public List<Locale> getSupportedLanguages() {
        if (!supportedLanguages.contains(defaultLanguage)) {
            supportedLanguages.add(defaultLanguage);
        }
        return supportedLanguages;
    }

    public void setSupportedLanguages(List<Locale> supportedLanguages) {
        this.supportedLanguages = supportedLanguages;
    }

    public String getDefaultCountryCode() {
        return defaultCountryCode;
    }

    public void setDefaultCountryCode(String defaultCountryCode) {
        this.defaultCountryCode = defaultCountryCode;
    }

    public List<String> getSupportedCountryCodes() {
        return supportedCountryCodes;
    }

    public void setSupportedCountryCodes(List<String> supportedCountryCodes) {
        this.supportedCountryCodes = supportedCountryCodes;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

}
