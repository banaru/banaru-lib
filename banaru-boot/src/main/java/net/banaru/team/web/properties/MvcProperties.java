package net.banaru.team.web.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * 롬복의 @Data를 이용하면 json에 두 개의 getter, setter가 생김, getter, setter의 직접 구현이 필요하다.
 * 
 * @author banssolin
 */
@ConfigurationProperties(prefix = "banaru.mvc")
public class MvcProperties {

    // view 라는 스트링이 yml과 일치해야 함
    @NestedConfigurationProperty
    private JspViewProperties jspView = new JspViewProperties();

    private String errorTemplatePath;

    public JspViewProperties getJspView() {
        return jspView;
    }

    // @NestedConfigurationProperty는 new로 생성 안의 setter를 이용하기 때문에, setter 필수 아님
    // public void setView(ViewProperties view) {
    // this.view = view;
    // }

    public String getErrorTemplatePath() {
        return errorTemplatePath;
    }

    // 보통 setter 필수
    public void setErrorTemplatePath(String errorTemplatePath) {
        this.errorTemplatePath = errorTemplatePath;
    }

}
