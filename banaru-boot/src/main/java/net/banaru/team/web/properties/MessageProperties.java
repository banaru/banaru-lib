package net.banaru.team.web.properties;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author banssolin
 *
 */
@ConfigurationProperties(prefix = "banaru.message")
public class MessageProperties {

    private String cookieName = "banaru.i18n.locale";

    private Integer cookieMaxAge = 60;

    private int cacheSeconds = 60;

    // TODO 초기화 안되면 어떻게 되는지 확인
    private List<String> basenames = new ArrayList<>();

    public String getCookieName() {
        return cookieName;
    }

    public void setCookieName(String cookieName) {
        this.cookieName = cookieName;
    }

    public Integer getCookieMaxAge() {
        return cookieMaxAge;
    }

    public void setCookieMaxAge(Integer cookieMaxAge) {
        this.cookieMaxAge = cookieMaxAge;
    }

    public int getCacheSeconds() {
        return cacheSeconds;
    }

    public void setCacheSeconds(int cacheSeconds) {
        this.cacheSeconds = cacheSeconds;
    }

    public List<String> getBasenames() {
        return basenames;
    }

    public void setBasenames(List<String> basenames) {
        this.basenames = basenames;
    }

}
