package net.banaru.team.logger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author banssolin
 *
 */
@Configuration
public class LoggerInjectorConfiguration {

    @Bean
    public LoggerPostProcessor loggerPostProcessor() {
        return new LoggerPostProcessor();
    }

}
