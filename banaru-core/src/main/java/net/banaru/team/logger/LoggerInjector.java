package net.banaru.team.logger;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@link org.springframework.context.annotation.Configuration} 어노테이션을 붙인 클래스에 {@link EnableLoggerInjector} 어노테이션을 붙인다.
 * &#64;LoggerInjector
 * private Logger logger;
 * 를 붙이면 slf4j룰 생성, 주입해 줌
 * 
 * 
 * @author banssolin
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Documented
public @interface LoggerInjector {

}
