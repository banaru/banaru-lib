package net.banaru.team.web.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CookieUtils {

    private static final Logger logger = LoggerFactory.getLogger(CookieUtils.class);
    private static final String DEFAULT_COOKIE_PATH = "/";

    /**
     * Create a cookie.
     *
     * @param name  the cookie name
     * @param value the value of the cookie to crate
     * @return the cookie
     */
    public static Cookie createCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        return cookie;
    }

    /**
     * Create a cookie.
     *
     * @param name         the cookie name
     * @param value        the value of the cookie to crate
     * @param cookieMaxAge the max age of the cookie
     * @return the cookie
     */
    public static Cookie createCookie(String name, String value, Integer cookieMaxAge) {
        Cookie cookie = createCookie(name, value);
        cookie.setMaxAge(cookieMaxAge);
        return cookie;
    }

    /**
     * Add a cookie.
     *
     * @param response the HTTP response
     * @param cookie   the cookie
     */
    public static void addCookie(HttpServletResponse response, Cookie cookie) {
        if (response == null) {
            throw new IllegalArgumentException("HttpServletResponse must not be null");
        }
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie must not be null");
        }
        if (cookie.getPath() == null || cookie.getPath().isEmpty()) {
            cookie.setPath(DEFAULT_COOKIE_PATH);
        }
        response.addCookie(cookie);
        if (logger.isDebugEnabled()) {
            logger.debug("Added cookie with name: {}, value: {}.", cookie.getName(), cookie.getValue());
        }
    }

    /**
     * Add a cookie.
     *
     * @param response the HTTP response
     * @param name     the cookie name
     * @param value    the value of the cookie to crate
     */
    public static void addCookie(HttpServletResponse response, String name, String value) {
        addCookie(response, createCookie(name, value));
    }

    /**
     * Add a cookie.
     *
     * @param response     the HTTP response
     * @param cookieName   the cookie name
     * @param cookieValue  the value
     * @param cookieMaxAge the max age of the cookie
     */
    public static void addCookie(HttpServletResponse response, String cookieName, String cookieValue,
                                 Integer cookieMaxAge) {
        addCookie(response, createCookie(cookieName, cookieValue, cookieMaxAge));
    }

    /**
     * Remove the cookie from the response. Will generate a cookie with empty value
     * and max age 0.
     *
     * @param response the HTTP response to remove the cookie
     * @param cookie   remove the cookie
     */
    public static void removeCooke(HttpServletResponse response, Cookie cookie) {
        if (response == null) {
            throw new IllegalArgumentException("HttpServletResponse must not be null");
        }
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie must not be null");
        }
        cookie.setValue("");
        cookie.setMaxAge(0);
        if (cookie.getPath() == null || cookie.getPath().isEmpty()) {
            cookie.setPath(DEFAULT_COOKIE_PATH);
        }
        response.addCookie(cookie);
        if (logger.isDebugEnabled()) {
            logger.debug("Removed cookie with name: {}", cookie.getName());
        }
    }

    /**
     * Remove the cookies from the response.
     *
     * @param request     the HTTP request
     * @param response    the HTTP response
     * @param cookieNames remove the name of the cookies
     */
    public static void removeCookie(HttpServletRequest request, HttpServletResponse response, String... cookieNames) {
        if (request == null) {
            throw new IllegalArgumentException("HttpServletRequest must not be null");
        }
        if (response == null) {
            throw new IllegalArgumentException("HttpServletResponse must not be null");
        }
        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                String cookieName = cookie.getName();
                for (String checkCookieName : cookieNames) {
                    if (checkCookieName.equals(cookieName)) {
                        removeCooke(response, cookie);
                    }
                }
            }
        }
    }

    /**
     * Get cookie.
     *
     * @param request    the HTTP request
     * @param cookieName the cookie name
     * @return the cookie
     */
    public static Cookie getCookie(HttpServletRequest request, String cookieName) {
        if (request == null) {
            throw new IllegalArgumentException("HttpServletRequest must not be null.");
        }
        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookieName.equals(cookie.getName())) {
                    return cookie;
                }
            }
        }
        return null;
    }

    /**
     * Get cookie value.
     *
     * @param request    the HTTP request
     * @param cookieName the cookie name
     * @return the cookie value
     */
    public static String getCookieValue(HttpServletRequest request, String cookieName) {
        Cookie cookie = getCookie(request, cookieName);
        return cookie == null ? null : cookie.getValue();
    }

}
