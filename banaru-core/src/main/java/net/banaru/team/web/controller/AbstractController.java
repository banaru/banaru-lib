package net.banaru.team.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import net.banaru.team.web.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractController {

    private static final String REDIRECT_PREFIX = "redirect:";

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    protected MessageUtils messageUtils;

    protected String getRedirectPath(String path) {
        return REDIRECT_PREFIX.concat(path);
    }

    protected void printErrors(BindingResult bindingResult) {
        if (!CollectionUtils.isEmpty(bindingResult.getGlobalErrors())) {
            logger.debug("=== Global errors===");
            bindingResult.getGlobalErrors().forEach(error -> {
                logger.debug("Form: [{}], Message: [{}]", error.getObjectName(), getErrorMessage(error));
            });
        }

        if (!CollectionUtils.isEmpty(bindingResult.getFieldErrors())) {
            logger.debug("=== Field errors===");
            bindingResult.getFieldErrors().forEach(error -> {
                logger.debug("Form: [{}], Field: [{}], Message: [{}]", error.getObjectName(), error.getField(),
                    getErrorMessage(error));
            });
        }
    }

    private String getErrorMessage(ObjectError error) {
        String errorMessage;
        if (StringUtils.isEmpty(error.getDefaultMessage())) {
            errorMessage = messageUtils.hasMessage(error.getCode()) ? messageUtils.getMessage(error.getCode())
                : null;
        } else {
            errorMessage = error.getDefaultMessage();
        }
        return errorMessage;
    }

}
