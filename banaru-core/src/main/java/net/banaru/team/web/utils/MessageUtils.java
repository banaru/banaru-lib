package net.banaru.team.web.utils;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * @author banssolin
 */
public class MessageUtils {

    private final MessageSource messageSource;

    public MessageUtils(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessage(String code, Object... args) {
        return getMessage(LocaleContextHolder.getLocale(), code, args);
    }

    public String getMessage(Locale locale, String code, Object... args) {
        if (!hasMessage(locale, code)) {
            return String.format("??%s_%s??", code, locale);
        }
        return messageSource.getMessage(code, args, locale);
    }

    public boolean hasMessage(String code) {
        return hasMessage(LocaleContextHolder.getLocale(), code);
    }

    public boolean hasMessage(Locale locale, String code) {
        try {
            messageSource.getMessage(code, null, locale);
        } catch (NoSuchMessageException e) {
            return false;
        }
        return true;
    }

    public MessageSource getMessageSource() {
        return messageSource;
    }

}
