
package net.banaru.team.web.dto;

import java.util.Locale;

/**
 * @author banssolin
 */
public class Language {

    private String name;
    private String langCode;
    private Locale locale;

    public Language(String name, Locale locale) {
        this.name = name;
        this.langCode = locale.getLanguage();
        this.locale = locale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

}
