package net.banaru.team.web.dto;

/**
 * @author banssolin
 */
public class Country {

    private String countryCode;
    private String displayName;

    public Country(String countryCode, String displayName) {
        this.countryCode = countryCode;
        this.displayName = displayName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

}
