package net.banaru.team.web.rest.connector;

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * @author banssolin
 *
 */
public class RestConnector {

    public static <RESPONSE_TYPE> RestGetExecutor<RESPONSE_TYPE> get(String path, Class<RESPONSE_TYPE> responseType) {
        return new RestGetExecutor<RESPONSE_TYPE>(getRestTemplate(), path, responseType);
    }

    public static <RESPONSE_TYPE> RestGetExecutor<RESPONSE_TYPE> get(RestTemplate restTemplate, String path,
            Class<RESPONSE_TYPE> responseType) {
        return new RestGetExecutor<RESPONSE_TYPE>(restTemplate, path, responseType);
    }

    public static <RESPONSE_TYPE> RestPostExecutor<RESPONSE_TYPE> post(String path, Class<RESPONSE_TYPE> responseType) {
        return new RestPostExecutor<RESPONSE_TYPE>(getRestTemplate(), path, responseType);
    }

    public static <RESPONSE_TYPE> RestPostExecutor<RESPONSE_TYPE> post(RestTemplate restTemplate, String path,
            Class<RESPONSE_TYPE> responseType) {
        return new RestPostExecutor<RESPONSE_TYPE>(restTemplate, path, responseType);
    }

    public static RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        // library만 가지고 있으면 알아서 변환해 줌.
        // builder.modules(new JavaTimeModule());
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter(builder.build()));
        return restTemplate;
    }

}
