package net.banaru.team.web.rest.connector;

import java.net.URI;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * @author banssolin
 *
 */
abstract class AbstractRestExecutor<RESPONSE_TYPE> {

    protected final RestTemplate restTemplate;
    protected final Class<RESPONSE_TYPE> responseType;
    protected final UriComponentsBuilder builder;

    AbstractRestExecutor(RestTemplate restTemplate, String path, Class<RESPONSE_TYPE> responseType) {
        this.restTemplate = restTemplate;
        this.responseType = responseType;
        this.builder = UriComponentsBuilder.fromUriString(path);
    }

    public RESPONSE_TYPE execute() {
        return execute(false);
    }

    public RESPONSE_TYPE executeWithThrowable() {
        return execute(true);
    }

    protected URI getUri() {
        return builder.build().encode().toUri();
    }

    abstract ResponseEntity<RESPONSE_TYPE> executeConnect();

    private RESPONSE_TYPE execute(boolean isThrowable) {
        ResponseEntity<RESPONSE_TYPE> responseEntity = executeConnect();
        return checkHttpStatus(responseEntity, isThrowable);
    }

    private RESPONSE_TYPE checkHttpStatus(ResponseEntity<RESPONSE_TYPE> responseEntity, boolean isThrowable) {
        if (!HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            if (isThrowable) {
                if (HttpStatus.NOT_FOUND.equals(responseEntity.getStatusCode())) {
                    // throw error
                } else {
                    // throw unexpected
                }
            } else {
                return null;
            }
        }
        return responseEntity.getBody();
    }

}
