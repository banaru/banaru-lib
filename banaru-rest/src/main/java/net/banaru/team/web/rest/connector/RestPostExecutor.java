package net.banaru.team.web.rest.connector;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * @author banssolin
 *
 */
public class RestPostExecutor<RESPONSE_TYPE> extends AbstractRestExecutor<RESPONSE_TYPE> {

    private Object target;

    RestPostExecutor(RestTemplate restTemplate, String path, Class<RESPONSE_TYPE> responseType) {
        super(restTemplate, path, responseType);
    }

    public RestPostExecutor<RESPONSE_TYPE> addTarget(Object target) {
        this.target = target;
        return this;
    }

    @Override
    ResponseEntity<RESPONSE_TYPE> executeConnect() {
        return restTemplate.postForEntity(getUri(), target, responseType);
    }

}
