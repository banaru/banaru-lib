package net.banaru.team.web.rest.connector;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * @author banssolin
 *
 */
public class RestGetExecutor<RESPONSE_TYPE> extends AbstractRestExecutor<RESPONSE_TYPE> {

    RestGetExecutor(RestTemplate restTemplate, String path, Class<RESPONSE_TYPE> responseType) {
        super(restTemplate, path, responseType);
    }

    public RestGetExecutor<RESPONSE_TYPE> addParam(String name, Object value) {
        builder.queryParam(name, value);
        return this;
    }

    public RestGetExecutor<RESPONSE_TYPE> addParams(String name, Object... values) {
        builder.queryParam(name, values);
        return this;
    }

    public RestGetExecutor<RESPONSE_TYPE> addParams(MultiValueMap<String, String> paramMap) {
        builder.queryParams(paramMap);
        return this;
    }

    @Override
    public ResponseEntity<RESPONSE_TYPE> executeConnect() {
        return restTemplate.getForEntity(getUri(), responseType);
    }

}
