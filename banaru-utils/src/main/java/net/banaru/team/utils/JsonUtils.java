package net.banaru.team.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author banssolin
 */
public class JsonUtils {

    private static final ObjectMapper mapper;
    private static final Logger logger = LoggerFactory.getLogger(net.banaru.team.utils.JsonUtils.class);

    static {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static String toJson(Object object, boolean isPretty) {
        String json = null;
        try {
            if (isPretty)
                json = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(object);
            else
                json = mapper.writeValueAsString(object);
        } catch (JsonProcessingException exception) {
            logger.error(exception.getMessage(), exception);
        }
        return json;
    }

    public static String toPrettyJson(Object object) {
        return toJson(object, true);
    }

}
